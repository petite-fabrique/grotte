# Grotte

## Setup
- RPI : image malinette modif rc.local, bashrc
- RPI DAC+ : /boot/config.txt, /etc/asound.conf
- Démarrage : /etc/rc.local
- Lancement : sh /home/pi/malinette-soft-rpi-master/start-nogui.sh
- Patch : /home/pi/malinette-soft-rpi-master/malinette-ide/projects/install/main.pd

! Attention: fichiers wav trop lours pour git, à mettre dans : /home/pi/malinette-soft-rpi-master/malinette-ide/projects/install/data/