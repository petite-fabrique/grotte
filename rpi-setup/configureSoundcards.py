#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re, subprocess

def runShell(cmd):
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   output = p.communicate()[0]
   return output


def configureSoundCards():
    for i in range(0,5) :
        sControls = runShell("amixer -c"+str(i)+" scontrols").decode()
        if "Invalid card number." in sControls : return
        print("setting up card ",i)
        before, after = "Simple mixer control '", "',"
        for control in sControls.split("\n"):
            control = re.search(before+'(.+)'+after, control)
            if control :
                control = control.group(1)
                print("  setting up ", control, " to 100% : ", runShell("amixer -c"+str(i)+" -q sset '"+control+"' 100%"))

if __name__ == "__main__":
    configureSoundCards()
    raise SystemExit
